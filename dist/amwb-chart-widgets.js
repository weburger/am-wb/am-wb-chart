/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * app module
 *
 * author: mgh
 */
angular.module('ngMaterialWeburgerChart', [
    'ngMaterialWeburger',
    'nvd3'
]);


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('ngMaterialWeburgerChart')
/**
 * 
 */

.config(['ngMdIconServiceProvider', function(ngMdIconServiceProvider) {
	ngMdIconServiceProvider
	// Move actions
	.addShape('wb-blank', '')
	.addShape('wb-common-moveup', '<path d="M7.41,15.41L12,10.83L16.59,15.41L18,14L12,8L6,14L7.41,15.41Z" />')
	.addShape('wb-action-add', '<path  d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" />')
	.addShape('wb-action-close', ' <path  d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2C6.47,2 2,6.47 2,12C2,17.53 6.47,22 12,22C17.53,22 22,17.53 22,12C22,6.47 17.53,2 12,2M14.59,8L12,10.59L9.41,8L8,9.41L10.59,12L8,14.59L9.41,16L12,13.41L14.59,16L16,14.59L13.41,12L16,9.41L14.59,8Z" />')
	.addShape('wb-action-edit', '<path  d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />')
	.addShape('wb-object-database', '    <path  d="M12,3C7.58,3 4,4.79 4,7C4,9.21 7.58,11 12,11C16.42,11 20,9.21 20,7C20,4.79 16.42,3 12,3M4,9V12C4,14.21 7.58,16 12,16C16.42,16 20,14.21 20,12V9C20,11.21 16.42,13 12,13C7.58,13 4,11.21 4,9M4,14V17C4,19.21 7.58,21 12,21C16.42,21 20,19.21 20,17V14C20,16.21 16.42,18 12,18C7.58,18 4,16.21 4,14Z" />')
	.addShape('wb-action-add-database', ' <path  d="M9,3C4.58,3 1,4.79 1,7C1,9.21 4.58,11 9,11C13.42,11 17,9.21 17,7C17,4.79 13.42,3 9,3M1,9V12C1,14.21 4.58,16 9,16C13.42,16 17,14.21 17,12V9C17,11.21 13.42,13 9,13C4.58,13 1,11.21 1,9M1,14V17C1,19.21 4.58,21 9,21C10.41,21 11.79,20.81 13,20.46V17.46C11.79,17.81 10.41,18 9,18C4.58,18 1,16.21 1,14M18,14V17H15V19H18V22H20V19H23V17H20V14" />')
	.addShapes({
		
		'wb-widget-chart': ngMdIconServiceProvider.getShape('multiline_chart'),
		'wb-widget-chart-pie': ngMdIconServiceProvider.getShape('pie_chart'),
		'wb-widget-chart-box': ngMdIconServiceProvider.getShape('insert_chart'),
	});
	
	
	;
}])
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * general setting for the chart-pie widget
 *
 * author mgh
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartBoxSettingCtrl', function ($scope) {
	var ngModel = $scope.wbModel;

	// define all chart types which are supported by the widget
	$scope.chartTypes = [{
		title: 'Box Plot Chart',
		icon: 'wb-horizontal-boxes',
		type: 'single',
		value: 'boxPlotChart'
	}];
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Chart-line-bar module
 *
 * author: mgh
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartBoxWidgetCtrl', function($scope, $amWbChartPrometheus,$amWbChartDataUtil, $q) {

	var ngModel = $scope.wbModel;

	if (!angular.isDefined(ngModel.chart)) {
		ngModel.chart = {
				type : 'boxPlotChart',
				height : 450,
				margin : {
					top : 20,
					right : 20,
					bottom : 60,
					left : 40
				},
				color : [ 'darkblue', 'darkorange', 'green', 'darkred', 'darkviolet' ],
				// y: function(d){return d.values.Q3;},
				maxBoxWidth : 75,
		};
	}

	ngModel.chart.x = function(d) {
		return d[0];
	};

	ngModel.chart.q1 = function(d) {
		return d[1]
	};
	ngModel.chart.q2 = function(d) {
		return d[2]
	};
	ngModel.chart.q3 = function(d) {
		return d[3]
	};
	ngModel.chart.wl = function(d) {
		return d[4]
	};
	ngModel.chart.wh = function(d) {
		return d[5]
	};

	ngModel.chart.outliers= function (d) { 
		if(d.length <= 6){
			return [];
		}
		return d.slice(6, d.length);
	}


	/**
	 * Load widget init data
	 */
	function loadPreData() {
		var result = null;
		if(angular.isDefined(ngModel.query)){
			result = $amWbChartPrometheus.run(ngModel.query)//
			.then(function(data){
				for(var i = 0; i < data.length; i++){
					$amWbChartDataUtil.appendTextToColumn(data[i].key, 0, data[i]);
				}
				ngModel.data = $amWbChartDataUtil.appendSheetsAsRow(data);
			});
		} else {
			ngModel.data = {
					key : "Box data",
					values : [
						//[label, Q1, Q2, Q3, low, high, ... outliers]
						[ 'label1', 100, 200, 300, 50, 350, 10, 360, 355],
						[ 'label2', 100, 200, 300, 50, 350, 10, 360, 355 ],
						[ 'label3', 100, 200, 300, 50, 350, 10, 360, 355 ]
						]
			};
		}
		if(ngModel.searchReplace){
			result = $q.when(result)//
			.then(function(){
				if(angular.isArray(ngModel.searchReplace)){
					angular.forEach(ngModel.searchReplace, function(option){
						$amWbChartDataUtil.findAndReplace(ngModel.data, option);
					});
				} else {
					$amWbChartDataUtil.findAndReplace(ngModel.data, ngModel.searchReplace);
				}
			});
		}
	}

	if (!angular.isDefined(ngModel.data)) {
		loadPreData();
	}
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Chart-line-bar module
 *
 * author: mgh
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbMultiChartWidgetCtrl', function ($scope, $amWbChartPrometheus, $amWbChartDataUtil, $q) {
	var ngModel = $scope.wbModel;

	/*
	 * Chart option,
	 * 
	 * User options must be merged with to show user setting 
	 * effects.
	 */
	if(!angular.isDefined(ngModel.chart)){
		ngModel.chart = {
			type: 'multiChart',
			height: 450,
			margin: {
				top: 50,
				right: 75,
				bottom: 50,
				left: 75
			},
			useVoronoi: false,
			clipEdge: false,
			duration: 250,
			useInteractiveGuideline: true,
			xAxis: {
				showMaxMin: false,
			},
			yAxis1: {
				showMaxMin: false,
			},
			yAxis2: {
				showMaxMin: false,
			}
		} ;
	}
	/**
	 * Load x value
	 */
	ngModel.chart.x = function (data) {
		return data[0];
	};

	/**
	 * Load Y value
	 */
	ngModel.chart.y = function (data) {
		return data[1];
	};

	/**
	 * X-Axis value formatter
	 */
	ngModel.chart.xAxis.tickFormat = function (data) {
		if (!angular.isDefined(ngModel.xformat)){
			return data;
		}
		// Adjust data
		if (ngModel.xformat.adjust) {
			var min = ngModel.data[0].values[0][0];
			ngModel.data.forEach(function (item) {
				if(min > item.values[0][0]){
					min = item.values[0][0];
				}
			});
			data = data - min;
		}
		// Format data
		return $amWbChartDataUtil.formatData(data, ngModel.xformat);
	};

	/**
	 * Y-Axis value formatter
	 */
	ngModel.chart.yAxis1.tickFormat = function (data) {
		if (!ngModel.yformat1)
			return data;
		return $amWbChartDataUtil.formatData(data, ngModel.yformat1);
	};
	ngModel.chart.yAxis2.tickFormat = function (data) {
		if (!ngModel.yformat2)
			return data;
		return $amWbChartDataUtil.formatData(data, ngModel.yformat2);
	};

	/**
	 * Loads default data
	 * 
	 */
	function loadPreData() {
		var result = null;
		if(angular.isDefined(ngModel.query)){
			result = $amWbChartPrometheus.run(ngModel.query, false)//
			.then(function(data){
				ngModel.data = data;
				angular.forEach(ngModel.data, function(sheet, key){
					sheet.type = 'line';
					if(ngModel.yAxis2Match){
						var regex = new RegExp(ngModel.yAxis2Match, 'ig');
						if(regex.test(sheet.key)){
							sheet.yAxis = 2;
						} else {
							sheet.yAxis = 1;
						}
					} else {
						sheet.yAxis = (key % 2)+1;
					}
				});
			});
		} else {
			ngModel.data = [{
				key: 'key1',
				color: 'red',
				type: 'line',
				values: [[0, 0], [1, 1000], [2, 2000], [3, 3000], [4, 4000], [5, 5000], [6,6000]]
			}, {
				key: 'key2',
				color: 'blue',
				type: 'line',
				values: [[0, 0], [1, 2], [2, 4], [3, 8], [4, 16], [5, 32], [6, 64]]
			}];
		}
		if(ngModel.rename){
			$q.when(result)//
			.then(function(){
				angular.forEach(ngModel.data, function(sheet){
					for(var i = 0; i < ngModel.rename.length; i++){
						var rule = ngModel.rename[i];
						var regex = new RegExp(rule.query, 'ig');
						if(regex.test(sheet.key)){
							sheet.key = rule.replace;
							break;
						}
					}
				});
			});
		}
	}

	if (! angular.isArray(ngModel.data)){
		loadPreData();
	}
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * setting for X and Y Axis of the graph widget
 *
 * author mgh
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartLineBarSettingAxisCtrl', function($scope) {
	var ngModel = $scope.wbModel;

	$scope.formats =[{
		title : 'Date time',
		icon : 'wb-vertical-boxes',
		value : 'DateTime',
	},{
		title : 'Number',
		icon : 'wb-vertical-boxes',
		value : 'Number',
	}];

	var patterns = {
			DateTime : [{
				title : '2017-09-25',
				icon : 'wb-vertical-boxes',
				value : '%Y-%m-%d'
			}, {
				title : '2017-09-25T08:15:30',
				icon : 'wb-vertical-boxes',
				value : '%Y-%m-%dT%H:%M:%S'
			}, {
				title : '08:15:30',
				icon : 'wb-vertical-boxes',
				value : '%H:%M:%S'
			}],
			Number : [{
				title : 'x',
				icon : 'wb-vertical-boxes',
				value : '.0f'
			}, {
				title : '.x',
				icon : 'wb-vertical-boxes',
				value : '.1f'
			}, {
				title : '.xx',
				icon : 'wb-horizontal-boxes',
				value : '.2f'
			}, {
				title : '.xxx',
				icon : 'wb-horizontal-boxes',
				value : '.3f'
			}, {
				title : 'K/M/G/T',
				icon : 'wb-horizontal-boxes',
				value : '.3s'
			}, {
				title : 'Thousands',
				icon : 'wb-horizontal-boxes',
				value : ',.2r'
			}]
	}
	
	/**
	 * Get patterns of a format
	 */
	$scope.getPatterns = function (type){
		return patterns[type];
	}

});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * general setting for the graph widget
 *
 * author mgh
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartLineBarSettingCtrl', function ($scope) {
	var ngModel = $scope.wbModel;

	// define all graph types which are supported by the widget
	$scope.chartTypes = [{
		title: 'Multi-Line',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'lineChart'
	}, {
		title: 'Bars',
		icon: 'wb-vertical-boxes',
		type: 'single',
		value: 'discreteBarChart'
	}, {
		title: 'Multi-Bars',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'multiBarChart'
	}, {
		title: 'Horizontal Bars',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'multiBarHorizontalChart'
	}];

	/**
	 * creates new data node and adds it to the graph-data which is bound to the nvd3-graph widget
	 */
	$scope.addGraphData = function () {
		if (!ngModel.graphdata)
			ngModel.graphdata = [];
		ngModel.graphdata.push({
			key: 'your-label',
			color: 'red',
			values: {}
		})
	};

	/**
	 * remove a data node from graph-data which is bound to the nvd3-graph widget
	 * @param ds
	 */
	$scope.removeGraphData = function (ds) {
		var index = $scope.wbModel.graphdata.indexOf(ds);
		if (index > -1) {
			$scope.wbModel.graphdata.splice(index, 1);
		}
	}
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Chart-line-bar module
 *
 * author: mgh
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartLineBarWidgetCtrl', function ($scope, $amWbChartPrometheus, $amWbChartDataUtil, $q) {
	var ngModel = $scope.wbModel;

	/*
	 * Chart option,
	 * 
	 * User options must be merged with to show user setting 
	 * effects.
	 */
	if(!angular.isDefined(ngModel.chart)){
		ngModel.chart = {
			type: 'lineChart',
			height: 450,
			margin: {
				top: 50,
				right: 75,
				bottom: 50,
				left: 75
			},
			useVoronoi: false,
			clipEdge: false,
			duration: 250,
			useInteractiveGuideline: true,
			xAxis: {
				showMaxMin: false,
			},
			yAxis: {
				showMaxMin: false,
			}
		} ;
	}
	/**
	 * Load x value
	 */
	ngModel.chart.x = function (data) {
		return data[0];
	};

	/**
	 * Load Y value
	 */
	ngModel.chart.y = function (data) {
		return data[1];
	};

	/**
	 * X-Axis value formatter
	 */
	ngModel.chart.xAxis.tickFormat = function (data) {
		if (!angular.isDefined(ngModel.xformat)){
			return data;
		}
		// Adjust data
		if (ngModel.xformat.adjust) {
			var min = ngModel.data[0].values[0][0];
			ngModel.data.forEach(function (item) {
				if(min > item.values[0][0]){
					min = item.values[0][0];
				}
			});
			data = data - min;
		}
		// Format data
		return $amWbChartDataUtil.formatData(data, ngModel.xformat);
	};

	/**
	 * Y-Axis value formatter
	 */
	ngModel.chart.yAxis.tickFormat = function (data) {
		if (!ngModel.yformat)
			return data;
		return $amWbChartDataUtil.formatData(data, ngModel.yformat);
	};

	/**
	 * Loads default data
	 * 
	 */
	function loadPreData() {
		var result = null;
		if(angular.isDefined(ngModel.query)){
			result = $amWbChartPrometheus.run(ngModel.query, false)//
			.then(function(data){
				ngModel.data = data;
			});
		} else {
			ngModel.data = [{
				key: 'key1',
				color: 'red',
				values: [[0, 0], [1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6,6]]
			}, {
				key: 'key2',
				color: 'blue',
				values: [[0, 0], [1, 2], [2, 4], [3, 8], [4, 16], [5, 32], [6, 64]]
			}];
		}
		if(ngModel.rename){
			$q.when(result)//
			.then(function(){
				angular.forEach(ngModel.data, function(sheet){
					for(var i = 0; i < ngModel.rename.length; i++){
						var rule = ngModel.rename[i];
						var regex = new RegExp(rule.query, 'ig');
						if(regex.test(sheet.key)){
							sheet.key = rule.replace;
							break;
						}
					}
				});
			});
		}
	}

	if (! angular.isArray(ngModel.data)){
		loadPreData();
	}
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * main setting module for data-query
 *
 * author: mgh
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartDataMultiSettingCtrl', function($scope, $wbUi) {
	var ngModel = $scope.wbModel;
	
	$scope.chartTypes = [{
		title: 'Line',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'lineChart'
	}, {
		title: 'Bars',
		icon: 'wb-vertical-boxes',
		type: 'single',
		value: 'discreteBarChart'
	}, {
		title: 'Multi-Bars',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'multiBarChart'
	}, {
		title: 'Horizontal Bars',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'multiBarHorizontalChart'
	}];

	function addExampleData() {
		if (!angular.isDefined(ngModel.data)) {
			ngModel.data = [];
		}
		ngModel.data.push({
			key : 'New data',
			query : {},
			values : [ [ 1, 1 ], [ 2, 2 ] ]
		});
	}

	function removeDataItem(ds) {
		var index = ngModel.data.indexOf(ds);
		if (index > -1) {
			ngModel.data.splice(index, 1);
		}
	}

	$scope.addData = addExampleData;
	$scope.removeData = removeDataItem;
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * general setting for the chart-pie widget
 *
 * author mgh
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartPieSettingCtrl', function ($scope) {
	var ngModel = $scope.wbModel;

	// define all chart types which are supported by the widget
	$scope.chartTypes = [{
		title: 'Pie Chart',
		icon: 'wb-horizontal-boxes',
		type: 'single',
		value: 'pieChart'
	}, {
		title: 'Donut Chart',
		icon: 'wb-vertical-boxes',
		type: 'single',
		value: 'donutChart'
	}];

});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Chart-Pie widget module
 *
 * author: mgh
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('ngMaterialWeburgerChart')
	/**
	 *
	 */
	.controller('AmWbChartPieWidgetCtrl', function($scope, $amWbChartPrometheus, $amWbChartDataUtil, $q) {
		var ngModel = $scope.wbModel;

		//default chart options bind to nvd3-graph widget
		if (!angular.isDefined(ngModel)) {
			ngModel = {
				chart : {
					type : 'pieChart',
					height : 450,
					margin : {
						top : 50,
						right : 75,
						bottom : 50,
						left : 75
					},
					showLabels : true,
					labelSunbeamLayout : true
				}
			};
		}

		ngModel.chart.x = function(d) {
			return d[0];
		};

		ngModel.chart.y = function(d) {
			return d[1];
		};

		//check for 'donut' mode
		$scope.$watchCollection('[wbModel.chart.donut]', function() {
			if (ngModel.chart.donut === true) {
				ngModel.chart.pie = {
					startAngle : function(d) {
						return d.startAngle / 2 - Math.PI / 2
					},
					endAngle : function(d) {
						return d.endAngle / 2 - Math.PI / 2
					}
				};
			} else {
				ngModel.chart.pie = {};
			}
		});


		/**
		 * Load sample data
		 */
		function loadPreData() {
			var result = null;
			if(angular.isDefined(ngModel.query)){
				result = $amWbChartPrometheus.run(ngModel.query, false)//
				.then(function(data){
					for(var i = 0; i < data.length; i++){
						$amWbChartDataUtil.appendTextToColumn(data[i].key, 0, data[i]);
					}
					ngModel.data = $amWbChartDataUtil.appendSheetsAsRow(data);
				});
			} else {
				ngModel.data = {
						key : 'Pie sample',
						values : [ [ 'A', 10 ], [ 'B', 10 ], [ 'C', 10 ] ]
				};
			}
			if(ngModel.searchReplace){
				result = $q.when(result)//
				.then(function(){
					if(angular.isArray(ngModel.searchReplace)){
						angular.forEach(ngModel.searchReplace, function(option){
							$amWbChartDataUtil.findAndReplace(ngModel.data, option);
						});
					} else {
						$amWbChartDataUtil.findAndReplace(ngModel.data, ngModel.searchReplace);
					}
				});
			}
		}

		if (!angular.isDefined(ngModel.data)) {
			loadPreData();
		}
	});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Table widget module
 *
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbTableWidgetCtrl', function($scope, $amWbChartPrometheus, $amWbChartDataUtil, $q) {
	var ngModel = $scope.wbModel;

	/**
	 * Load sample data
	 */
	function loadPreData() {
		var result = null;
		if(angular.isDefined(ngModel.query)){
			result = $amWbChartPrometheus.run(ngModel.query, false)//
			.then(function(data){
				if(angular.isArray(data)){
					for(var i = 0; i < data.length; i++){
						$amWbChartDataUtil.appendTextToColumn(data[i].key, 0, data[i]);
					}
					data = $amWbChartDataUtil.appendSheetsAsRow(data);
				}
				ngModel.data = data;
			});
		} else {
			ngModel.data = {
					key : 'Table sample',
					values : [ [ 'A', 10 ], [ 'B', 10 ], [ 'C', 10 ] ]
			}
		}
		if(ngModel.searchReplace){
			result = $q.when(result)//
			.then(function(){
				if(angular.isArray(ngModel.searchReplace)){
					angular.forEach(ngModel.searchReplace, function(option){
						$amWbChartDataUtil.findAndReplace(ngModel.data, option);
					});
				} else {
					$amWbChartDataUtil.findAndReplace(ngModel.data, ngModel.searchReplace);
				}
			});
		}
	}

	if (!angular.isDefined(ngModel.data)) {
		loadPreData();
	}
});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerChart')

    /**
     * @ngdoc directive
     * @name wbUiSettingDropdown
     * @memberof ngMaterialWeburger
     * @description a setting section for choosing values.
     *
     */
    .directive('wbUiSettingDropdownValue', function () {
        return {
            templateUrl: 'views/directives/wb-ui-setting-dropdown-value.html',
            restrict: 'E',
            scope: {
                title: '@title',
                value: '=value',
                icon: '@icon',
                items:'=items'
            }
        };
    });

/**
 * Created by mgh on 2/26/17.
 */
angular.module('ngMaterialWeburgerChart')

    /**
     * @ngdoc directive
     * @name wbUiSettingNumber
     * @memberof ngMaterialWeburger
     * @description a setting section to set a number.
     *
     */
    .directive('wbUiSettingText', function () {
        return {
            templateUrl: 'views/directives/wb-ui-setting-text.html',
            restrict: 'E',
            scope: {
                title: '@title',
                value: '=value',
                icon: '@icon'
            }
        };
    });

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Load widgets settigns
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('ngMaterialWeburgerChart')

/**
 * Load widgets
 *
 * author: mgh
 */
.run(function($settings) {

	$settings.newPage({
		type: 'table',
		label : 'Table',
		description : 'Manages a table properties',
		templateUrl : 'views/am-wb-chart-settings/table.html'
	});
	
	$settings.newPage({
		type: 'chart-legend',
		label : 'Legend',
		description : 'Configuration of chart legend',
		templateUrl : 'views/am-wb-chart-settings/legend.html'
	});
	$settings.newPage({
		type: 'chart-multi-chart',
		label : 'Multi chart',
		controller: 'AmWbChartDataMultiSettingCtrl',
		description : 'Multi chart configuration',
		templateUrl : 'views/am-wb-chart-settings/multi-chart.html'
	});
	
	//=== SECTION: CHART-GENERAL SETTINGS ===
	//chart-general title setting section
	$settings.newPage({
		type: 'chart-title',
		label : 'Chart - Title',
		description : 'Manage title setting for any chart widget',
		templateUrl : 'views/am-wb-chart-settings/title.html'
	});
	//chart-general caption setting section
	$settings.newPage({
		type: 'chart-caption',
		label : 'Chart - Caption',
		description : 'Manage caption setting for any Gazmeh chart widget',
		templateUrl : 'views/am-wb-chart-settings/caption.html'
	});
	//chart-general margin setting section
	$settings.newPage({
		type: 'chart-margin',
		label : 'Chart - Margin',
		description : 'Manage margin setting for any graph widget',
		templateUrl : 'views/am-wb-chart-settings/margin.html'
	});


	//=== SECTION: CHART-LINE-BAR SETTINGS ===
	//chart-line-bar main setting section
	$settings.newPage({
		type: 'chart-line-multi',
		label : 'Chart - Main',
		description : 'Manage main setting for chart-line-bar widget',
		controller:'AmWbChartDataMultiSettingCtrl',
		templateUrl : 'views/am-wb-chart-settings/line-multi.html'
	});
	
	//Multi data setting section
	$settings.newPage({
		type: 'chart-data-multi',
		label : 'Data sets',
		description : 'Manage data setting',
		controller:'AmWbChartDataMultiSettingCtrl',
		templateUrl : 'views/am-wb-chart-settings/data-multi.html'
	});

	//chart-line-bar axis setting section
	$settings.newPage({
		type: 'chart-axis',
		label : 'Chart - Axis',
		description : 'Manage axis setting for chart-line-bar widget',
		controller:'AmWbChartLineBarSettingAxisCtrl',
		templateUrl : 'views/am-wb-chart-settings/axis.html'
	});

	//=== SECTION: CHART-Pie SETTINGS ===
	//chart-pie main setting section
	$settings.newPage({
		type: 'chart-pie',
		label : 'Pie chart',
		description : 'Manage main setting for chart-pie widget',
		controller:'AmWbChartPieSettingCtrl',
		icon : 'settings',
		templateUrl : 'views/am-wb-chart-settings/pie.html'
	});

	//=== SECTION: CHART-Box SETTINGS ===
	//chart-pie main setting section
	$settings.newPage({
		type: 'chart-box',
		label : 'Box chart',
		description : 'Manage main setting for chart-pie widget',
		controller:'AmWbChartBoxSettingCtrl',
		icon : 'settings',
		templateUrl : 'views/am-wb-chart-settings/box.html'
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Loads widgets
 * 
 * 
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('ngMaterialWeburgerChart')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 *
 * author: mgh
 */
.run(function($widget) {

	$widget.newWidget({
		type : 'MultiChart',
		templateUrl : 'views/am-wb-chart-widgets/multi-chart.html',
		label : 'Multi chart',
		description : 'Multi chart (with two Y-axise) is used to show un-homugenuse data types.',
		icon : 'wb-widget-chart',
		help : 'https://gitlab.com/gazmeh',
		controller : 'AmWbMultiChartWidgetCtrl',
		setting : [
			'chart-margin',
			'chart-legend',
			'chart-title',
			'chart-caption',

			'chart-multi-chart'
			]
	});
	//Register 'chart-line-bar' widget
	$widget.newWidget({
		type : 'ChartLineBar',
		templateUrl : 'views/am-wb-chart-widgets/line-bar.html',
		label : 'Line chart',
		description : 'Line chart widget to display data in line/bar view.',
		icon : 'wb-widget-chart',
		help : 'https://gitlab.com/gazmeh',
		controller : 'AmWbChartLineBarWidgetCtrl',
		setting : [
			'chart-margin',
			'chart-legend',
			'chart-title',
			'chart-caption',

			'chart-line-multi',
			'chart-axis',
			]
	});


	//Register 'bar' widget
	$widget.newWidget({
		type : 'ChartBox',
		templateUrl : 'views/am-wb-chart-widgets/box.html',
		label : 'Box Chart',
		description : 'Box chart to show erros',
		icon : 'wb-widget-chart-box',
		help : 'https://gitlab.com/gazmeh',
		controller : 'AmWbChartBoxWidgetCtrl',
		setting : [
			'chart-margin',
			'chart-legend',
			'chart-title',
			'chart-caption',

			'chart-box',
			],
	});

	//Register 'pie' widget
	$widget.newWidget({
		type : 'ChartPie',
		templateUrl : 'views/am-wb-chart-widgets/pie.html',
		label : 'Pie Chart',
		description : 'Pie and variation of the pie chart like the donut chart',
		icon : 'wb-widget-chart-pie',
		help : 'https://gitlab.com/gazmeh',
		controller : 'AmWbChartPieWidgetCtrl',
		setting : [
			'chart-margin',
			'chart-legend',
			'chart-title',
			'chart-caption',

			'chart-pie',
			]
	});

	//Register 'pie' widget
	$widget.newWidget({
		type : 'Table',
		templateUrl : 'views/am-wb-chart-widgets/table.html',
		label : 'Table',
		description : 'Display a sheet of data as a table.',
		icon : 'border_all',
		help : 'https://gitlab.com/gazmeh',
		controller : 'AmWbTableWidgetCtrl',
		setting : [
			'table'
			]
	});

});
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerChart')

/**
 * @ngdoc service
 * @name $$amWbChartDataUtil
 * @memberof ngMaterialWeburgerChart
 * @description Data utility
 * 
 * Some basic data mainpulation tools.
 * 
 */
.service('$amWbChartDataUtil', function() {

	/**
	 * Format data as local date
	 * 
	 * Options:
	 * <ul>
	 * 	<li>type: data format (DateTime, )</li>
	 * 	<li>format: string to format data</li>
	 * </ul>
	 * 
	 * <h3>DateTime</h3>
	 * 
	 * In this case data must be a UnixTime in UTC, and the option format must 
	 * be as described here: 
	 * 
	 * http://pubs.opengroup.org/onlinepubs/009695399/functions/strptime.html
	 * 
	 * <h3>Default format</h3>
	 * 
	 * By default we suppose data is an number value and try to format as described
	 * here:
	 * 
	 * https://docs.python.org/3/library/string.html#format-specification-mini-language
	 * 
	 * 
	 * @param data value
	 * @param option to format data
	 */
	function formatData(data, option){
		if (option.type === 'DateTime'){
			var date = moment.unix(data);
			return d3.time.format(option.format || '%x')(date.toDate());
		}
		return d3.format(option.format || '.xx')(data);
	}
	
	/**
	 * Search and replace data in sheet
	 * 
	 * 
	 * query: a search query
	 * replace: a replacement
	 * range: where to search (may be empty)
	 * 
	 * match: true/false matche the case
	 * regex: true/false match based on regex
	 * formula: true/false search in formula
	 */
	function findAndReplace(sheet, option){
		// Replace with regex
		if(option.regex && option.match){
			var regex = new RegExp(option.query, 'ig');
			angular.forEach(sheet.values, function(row, i){
				angular.forEach(row, function(cell, j){
					if(regex.test(cell)){
						sheet.values[i][j] = option.replace;
					}
				});
			});
		}
		// TODO: support the others
	}
	
	
	function appendSheetsAsRow(dataSheet){
		var sheet = {
				key: 'New sheet',
				values:[]
		}
		for(var i = 0; i < dataSheet.length; i++){
			for(var j = 0; j < dataSheet[i].values.length; j++){
				sheet.values.push(dataSheet[i].values[j]);
			}
		}
		return sheet;
	}
	
	function appendTextToColumn(text, column, sheet){
		for(var i = 0; i < sheet.values.length; i++){
			sheet.values[i][column] = text + sheet.values[i][column];
		}
	}
	
	// public methods
	this.formatData = formatData;
	this.findAndReplace = findAndReplace; 
	this.appendSheetsAsRow = appendSheetsAsRow;
	this.appendTextToColumn = appendTextToColumn;
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerChart')

/**
 * @ngdoc service
 * @name $widget
 * @memberof ngMaterialWeburger
 * @description Resource managment
 * 
 */
.service('$amWbChartPrometheus', function($http, $q) {

	/**
	 * Convert string date into unix time
	 */
	function toUnixTime(dstr) {
		var date = moment //
		.utc(dstr);
		return date.unix();
	}

	/**
	 * 
	 */
	function parseValue(value) {
		var val = parseFloat(value);
		if (isNaN(val)) {
			// '+Inf', '-Inf', '+Inf' will be parsed into NaN by parseFloat(). The
			// can't be graphed, so show them as gaps (null).
			return null;
		}
		return val;
	}

	/**
	 * Remove HTML reserved char
	 */
	function escapeHTML(str) {
//		var entityMap = {
//				'&' : '&amp;',
//				'<' : '&lt;',
//				'>' : '&gt;',
//				'\'' : '&#39;',
//				'\"' : '&quot;',
//				'/' : '&#x2F;'
//		};
//
//		return String(string)//
//		.replace(/[&<>''\/]/g, function(s) {
//			return entityMap[s];
//		});
		return str;
	}

	/**
	 * 
	 */
	function metricToTsName(labels) {
		var tsName = (labels.__name__ || '') + '{';
		var labelStrings = [];
		for (var label in labels) {
			if (label !== '__name__') {
				labelStrings.push(label + '=\'' + labels[label] + '\'');
			}
		}
		tsName += labelStrings.join(',') + '}';
		return tsName;
	}
	
	
	
	function join(lookupTable, mainTable, lookupKey, mainKey, select) {
	    var l = lookupTable.length,
	        m = mainTable.length,
	        lookupIndex = [],
	        output = [];
	    for (var i = 0; i < l; i++) { // loop through l items
	        var row = lookupTable[i];
	        lookupIndex[row[lookupKey]] = row; // create an index for lookup table
	    }
	    for (var j = 0; j < m; j++) { // loop through m items
	        var y = mainTable[j];
	        var x = lookupIndex[y[mainKey]]; // get corresponding row from lookupTable
	        // Remove non matche items
	        if(angular.isDefined(x) && angular.isDefined(y)){
	        	output.push(select(y, x)); // select only the columns you need
	        } else {
	        	var w;
	        	w = 1;
	        }
	    }
	    return output;
	};
	
	function concatSheets(sheets){
		if(!angular.isArray(sheets)){
			return sheets;
		}
		if(sheets.length < 2){
			return sheets[0];
		}
		var result = join(sheets[1].values, sheets[0].values, 0, 0, function(row1, row2) {
		    return [
		        row1[0],
		        row1[1],
		        row2[1],
		    ];
		});
		for(var i = 0x2; i < sheets.length; i++){
			result = join(sheets[i].values, result, 0, 0, function(row1, row2) {
			    row1.push(row2[1]);
			    return row1;
			});
		}
		return {
			key: 'Concatedt sheets',
			values: result
		};
	}

	/**
	 * Convert prometheus vector to 2D array:
	 * 
	 *     metric	, value
	 *     x		,x
	 *     
	 * and put it into the sheet format
	 */
	function vectorToSheet(json){
		if (json.resultType !== 'vector') {
			alert('Result is not of matrix type! Please enter a correct expression.');
			return [];
		}
		
		// To data [name, value]
		var values = json.result.map(function(ts) {
			var name;
			if (ts.metric === null) {
				name = 'scalar';
			} else {
				name = escapeHTML(metricToTsName(ts.metric));
			}
			return [name, parseValue(ts.value[1])];
		});
		
		return {
			name: 'vector',
			values: values
		};
	}

	function durationToSeccond(duration){
		// Duration format: [0-9]+[smhdwy]
		if(angular.isNumber(duration)){
			return duration;
		}
		var regex = /([0-9]+)s/gi;
		var match = regex.exec(duration);
		if(match){
			return match[1]*1.0;
		}
		regex = /([0-9]+)m/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60;
		}
		regex = /([0-9]+)h/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60*60;
		}
		regex = /([0-9]+)d/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60*60*24;
		}
		regex = /([0-9]+)w/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60*60*24*7;
		}
		regex = /([0-9]+)y/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60*60*24*365;
		}
	}
	
	/**
	 * 
	 */
	function matrixToSheet(params, json) {
		if (json.resultType !== 'matrix') {
			alert('Result is not of matrix type! Please enter a correct expression.');
			return [];
		}
		var data = json.result.map(function(ts) {
			var name;
			if (ts.metric === null) {
				name = 'scalar';
			} else {
				name = escapeHTML(metricToTsName(ts.metric));
			}
			return {
				key : name,
				values : ts.values.map(function(value) {
					return [value[0], parseValue(value[1])];
				}),
			};
		});
		data.forEach(function(s) {
			// Insert nulls for all missing steps.
			var newSeries = [];
			var pos = 0;
			var step = durationToSeccond(params.step);
			for (var t = params.start; t <= params.end; t += step) {
				// Allow for floating point inaccuracy.
				if (s.values.length > pos && s.values[pos][0] < t + step / 100) {
					newSeries.push(s.values[pos]);
					pos++;
				} else {
					newSeries.push([t,null]);
				}
			}
			s.values = newSeries;
		});
		return data;
	}

	/**
	 * Run query based on url.
	 */
	function queryRange(url, params){
		return $http({
			method: 'GET',
			url: url,
			params: params
		}).then(function(data){
			return matrixToSheet(params, data.data.data);
		});
	}

	/**
	 * Run query 
	 */
	function query(url, params){
		return $http({
			method: 'GET',
			url: url,
			params: params
		})
		.then(function(result){
			// maso, 2017: convert result to sheet.
			return vectorToSheet(result.data.data);
		});
	}
	
	function dataSheetToSheet(dataSheet){
		dataSheet.forEach(function(s){
			s.values.unshift(['time', s.key]);
		});
		return concatSheets(data);
	}


	/**
	 * 
	 * params:
	 * 
	 * - url
	 * - start
	 * - end
	 * - step
	 * - query: string or array of string
	 * - queryRange: string
	 * 
	 * 
	 * 
	 */
	function runQuery(param, singleSheet){
		if(angular.isDefined(param.query_range)){
			return queryRange(param.url || '/api/v1/monitor/query_range', {
				query: param.query_range,
				start: toUnixTime(param.start_dtime),
				end: toUnixTime(param.end_dtime),
				step: param.step,
			}).then(function(dataSheet){
				if(singleSheet){
					return dataSheetToSheet(dataSheet);
				}
				return dataSheet;
			});
		}
		if(angular.isArray(param.query)){
			// Run and mearge queries
			var results = [];
			var jobs = [];
			angular.forEach(param.query, function(value){
				jobs.push(query(param.url || '/api/v1/monitor/query',{
					query: value.query,
					time: toUnixTime(param.end_dtime),
				}).then(function(result){
					result.key = value.key || result.key;
					results.push(result);
				}));
			});
			return $q.all(jobs)//
			.then(function(){
				if(singleSheet){
					return dataSheetToSheet(results);
				}
				return results;
			});
		}

		return query(param.url || '/api/v1/monitor/query',{
			query: param.query,
			time: toUnixTime(param.end_dtime),
		});
	}


	this.run = runQuery;
	this.query = query;
	this.queryRange = queryRange;
	this.toUnixTime = toUnixTime;
});

angular.module('ngMaterialWeburgerChart').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-chart-dialogs/feature.html',
    "<md-dialog aria-label=\"Feature item config\" ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>Feature list item</h2> <span flex></span> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content> <md-tabs md-dynamic-height md-border-bottom> <md-tab label=Context> <md-content layout=column>  <wb-ui-setting-image title=Image value=model.image> </wb-ui-setting-image> <md-input-container> <label translate>Title</label> <input ng-model=model.title> </md-input-container> <md-input-container> <label translate>Text</label> <input ng-model=model.text> </md-input-container> </md-content> </md-tab> <md-tab label=Action> <md-content layout=column> <md-input-container> <label translate>Type</label> <input ng-model=model.action.type> </md-input-container> <md-input-container> <label translate>Link (URL)</label> <input ng-model=model.action.link> </md-input-container> <md-input-container> <label translate>Label</label> <input ng-model=model.action.label> </md-input-container> </md-content> </md-tab> </md-tabs> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/am-wb-chart-settings/axis.html',
    "<md-list class=wb-setting-panel>   <md-subheader class=md-no-sticky translate>X Axis</md-subheader> <wb-ui-setting-text title=X-Label icon=blur_on value=wbModel.chart.xAxis.axisLabel> </wb-ui-setting-text> <wb-ui-setting-number title=\"Distance label\" icon=blur_on value=wbModel.chart.xAxis.axisLabelDistance> </wb-ui-setting-number> <wb-ui-setting-on-off-switch title=\"Stagger Labels?\" icon=blur_on value=wbModel.chart.xAxis.staggerLabels> </wb-ui-setting-on-off-switch> <wb-ui-setting-number title=\"Rotate labels\" icon=blur_on value=wbModel.chart.xAxis.rotateLabels> </wb-ui-setting-number> <wb-ui-setting-on-off-switch title=\"Rotate Y labels\" icon=blur_on value=wbModel.chart.xAxis.rotateYLabel> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=\"Show Max/Min\" icon=blur_on value=wbModel.chart.xAxis.showMaxMin> </wb-ui-setting-on-off-switch> <wb-ui-setting-number title=Duration icon=blur_on value=wbModel.chart.xAxis.duration> </wb-ui-setting-number>  <wb-ui-setting-dropdown title=Format icon=blur_on value=wbModel.xformat.type items=formats> </wb-ui-setting-dropdown> <wb-ui-setting-dropdown ng-show=wbModel.xformat.type title=\"Format pattern\" icon=blur_on value=wbModel.xformat.format items=getPatterns(wbModel.xformat.type)> </wb-ui-setting-dropdown> <wb-ui-setting-on-off-switch title=\"Adjust to origin?\" icon=blur_on value=wbModel.xformat.adjust> </wb-ui-setting-on-off-switch>   <md-subheader class=md-no-sticky translate>Y Axis</md-subheader> <wb-ui-setting-text title=Y-Label icon=blur_on value=wbModel.chart.yAxis.axisLabel> </wb-ui-setting-text> <wb-ui-setting-number title=\"Distance label\" icon=blur_on value=wbModel.chart.yAxis.axisLabelDistance> </wb-ui-setting-number> <wb-ui-setting-on-off-switch title=\"Stagger Labels?\" icon=blur_on value=wbModel.chart.yAxis.staggerLabels> </wb-ui-setting-on-off-switch> <wb-ui-setting-number title=\"Rotate labels\" icon=blur_on value=wbModel.chart.yAxis.rotateLabels> </wb-ui-setting-number> <wb-ui-setting-on-off-switch title=\"Rotate Y labels\" icon=blur_on value=wbModel.chart.yAxis.rotateYLabel> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=\"Show Max/Min\" icon=blur_on value=wbModel.chart.yAxis.showMaxMin> </wb-ui-setting-on-off-switch> <wb-ui-setting-number title=Duration icon=blur_on value=wbModel.chart.yAxis.duration> </wb-ui-setting-number>  <wb-ui-setting-dropdown title=Format icon=blur_on value=wbModel.yformat.type items=formats> </wb-ui-setting-dropdown> <wb-ui-setting-dropdown ng-show=wbModel.yformat.type title=\"Format pattern\" icon=blur_on value=wbModel.yformat.format items=getPatterns(wbModel.yformat.type)> </wb-ui-setting-dropdown> <wb-ui-setting-on-off-switch title=\"Adjust to origin?\" icon=blur_on value=wbModel.yformat.adjust> </wb-ui-setting-on-off-switch> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/box.html',
    "<md-list class=wb-setting-panel>  <wb-ui-setting-data title=\"Chart data\" icon=wb-object-database value=wbModel.data> </wb-ui-setting-data> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/caption.html',
    "<md-list class=wb-setting-panel>  <wb-ui-setting-on-off-switch title=caption? icon=blur_on value=wbModel.caption.enable> </wb-ui-setting-on-off-switch>  <textarea ui-tinymce=tinymceOptions ng-model=wbModel.caption.html flex>\n" +
    "\t</textarea> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/data-multi.html',
    "<md-list class=wb-setting-panel> <md-list-item> <wb-icon>wb-object-database</wb-icon> <p>Datasources</p> <md-button class=\"md-icon-button md-primary\" aria-label=Add ng-click=addData()> <wb-icon>wb-action-add-database</wb-icon> </md-button> </md-list-item> <wb-ui-setting-data ng-repeat=\"item in wbModel.data\" title=item icon=wb-object-database value=item remove=removeData> </wb-ui-setting-data> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/legend.html',
    "<md-list class=wb-setting-panel>   <wb-ui-setting-on-off-switch title=\"Enable align\" icon=blur_on value=wbModel.chart.legend.align> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch ng-show=wbModel.chart.legend.align title=\"Right align\" icon=blur_on value=wbModel.chart.legend.rightAlign> </wb-ui-setting-on-off-switch> <wb-ui-setting-number title=\"Max legend key length\" icon=blur_on value=wbModel.chart.legend.maxKeyLength> </wb-ui-setting-number> <wb-ui-setting-dropdown title=Vers icon=wb-direction items=\"[{title: 'Classic', value:'classic'},{title: 'Furious', value:'furious'}]\" value=wbModel.chart.legend.vers> </wb-ui-setting-dropdown> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/line-multi.html',
    "<md-list class=wb-setting-panel>  <wb-ui-setting-dropdown title=Type icon=wb-direction items=chartTypes value=wbModel.chart.type> </wb-ui-setting-dropdown> <wb-ui-setting-number ng-show=wbModel.style.padding.isUniform title=Radius icon=rounded_corner value=wbModel.style.padding.uniform> </wb-ui-setting-number>  <md-list-item> <wb-icon>wb-object-database</wb-icon> <p>Lines</p> <md-button class=\"md-icon-button md-primary\" aria-label=Add ng-click=addData()> <wb-icon>wb-action-add</wb-icon> </md-button> </md-list-item>  <div ng-repeat=\"item in wbModel.data\"> <md-list-item style=\"background-color: #eee\"> <wb-icon>wb-blank</wb-icon> <p>Line {{ $index+1 }}</p> <md-button class=\"md-icon-button md-primary\" aria-label=Remove ng-click=removeData(item)> <wb-icon style=color:darkred>wb-action-close</wb-icon> </md-button> </md-list-item> <wb-ui-setting-color title=Color icon=format_color_fill value=item.color> </wb-ui-setting-color> <wb-ui-setting-data title=Data icon=wb-object-database value=item remove=removeData> </wb-ui-setting-data> <wb-ui-setting-on-off-switch title=\"Show Area?\" icon=blur_on value=item.area> </wb-ui-setting-on-off-switch> </div> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/margin.html',
    "<md-list class=wb-setting-panel>   <md-subheader class=md-no-sticky>Margin</md-subheader>   <wb-ui-setting-number title=Left icon=rounded_corner value=wbModel.chart.margin.left> </wb-ui-setting-number>  <wb-ui-setting-number title=Right icon=rounded_corner value=wbModel.chart.margin.right> </wb-ui-setting-number>  <wb-ui-setting-number title=Top icon=rounded_corner value=wbModel.chart.margin.top> </wb-ui-setting-number>  <wb-ui-setting-number title=Bottom icon=rounded_corner value=wbModel.chart.margin.bottom> </wb-ui-setting-number> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/multi-chart.html',
    "<md-list class=wb-setting-panel>  <md-list-item> <wb-icon>wb-object-database</wb-icon> <p>Lines</p> <md-button class=\"md-icon-button md-primary\" aria-label=Add ng-click=addData()> <wb-icon>wb-action-add</wb-icon> </md-button> </md-list-item>  <div ng-repeat=\"item in wbModel.data\"> <md-list-item style=\"background-color: #eee\"> <wb-icon>wb-blank</wb-icon> <p>Line {{ $index+1 }}</p> <md-button class=\"md-icon-button md-primary\" aria-label=Remove ng-click=removeData(item)> <wb-icon style=color:darkred>wb-action-close</wb-icon> </md-button> </md-list-item>  <wb-ui-setting-dropdown title=Type icon=wb-direction items=\"[{title:'Line', value:'line'},{title:'Bar', value:'bar'},{title:'Area', value:'area'}]\" value=item.type> </wb-ui-setting-dropdown>  <wb-ui-setting-choose title=\"Y Axis\" icon=wb-horizontal-arrows items=\"[{Title: 'yAxis 1', value: '1'}, {Title: 'yAxis 2', value: '2'}]\" ng-model=item.yAxis> </wb-ui-setting-choose> <wb-ui-setting-color title=Color icon=format_color_fill value=item.color> </wb-ui-setting-color> <wb-ui-setting-data title=Data icon=wb-object-database value=item remove=removeData> </wb-ui-setting-data> <wb-ui-setting-on-off-switch title=\"Show Area?\" icon=blur_on value=item.area> </wb-ui-setting-on-off-switch> </div> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/pie.html',
    "<md-list class=wb-setting-panel>  <wb-ui-setting-on-off-switch title=\"Rotate labels?\" icon=blur_on value=wbModel.chart.donut> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=\"Rotate labels?\" icon=blur_on value=wbModel.chart.labelSunbeamLayout> </wb-ui-setting-on-off-switch> <wb-ui-setting-data title=Data icon=wb-object-database value=wbModel.data> </wb-ui-setting-data> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/table.html',
    "<md-list class=wb-setting-panel>  <wb-ui-setting-data title=Data icon=wb-object-database value=wbModel.data> </wb-ui-setting-data> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-settings/title.html',
    "<md-list class=wb-setting-panel>  <wb-ui-setting-on-off-switch title=Transparent? icon=blur_on value=wbModel.title.enable> </wb-ui-setting-on-off-switch>  <textarea ui-tinymce=tinymceOptions ng-model=wbModel.title.html flex>\n" +
    "    </textarea> </md-list>"
  );


  $templateCache.put('views/am-wb-chart-widgets/box.html',
    "<nvd3 options=wbModel data=wbModel.data.values config=\"{refreshDataOnly: false}\" api=api> </nvd3>"
  );


  $templateCache.put('views/am-wb-chart-widgets/line-bar.html',
    "<nvd3 options=wbModel data=wbModel.data config=\"{refreshDataOnly: false}\" api=api> </nvd3>"
  );


  $templateCache.put('views/am-wb-chart-widgets/multi-chart.html',
    "<nvd3 options=wbModel data=wbModel.data config=\"{refreshDataOnly: false}\" api=api> </nvd3>"
  );


  $templateCache.put('views/am-wb-chart-widgets/pie.html',
    "<nvd3 options=wbModel data=wbModel.data.values config=\"{refreshDataOnly: false}\" api=api> </nvd3>"
  );


  $templateCache.put('views/am-wb-chart-widgets/table.html',
    "<table width=100%> <thead> <tr> <th ng-repeat=\"value in wbModel.data.values[0] track by $index\">{{value}}</th> </tr> </thead> <tbody> <tr ng-repeat=\"values in wbModel.data.values track by $index\" ng-if=\"$index >= 1\"> <th ng-repeat=\"value in values track by $index\">{{value}}</th> </tr> </tbody>  </table>"
  );


  $templateCache.put('views/directives/wb-ui-setting-dropdown-value.html',
    " <md-list-item> <wb-icon ng-hide=\"icon==undefined || icon==null || icon==''\">{{icon}}</wb-icon> <p ng-hide=\"title==undefined || title==null || title==''\">{{title}}</p> <md-select style=\"margin: 0px\" ng-model=value> <md-option ng-repeat=\"item in items\" ng-value=item.value> {{item.title}} </md-option> </md-select> </md-list-item>"
  );


  $templateCache.put('views/directives/wb-ui-setting-text.html',
    "<md-list-item> <wb-icon ng-hide=\"icon==undefined || icon==null || icon==''\">{{icon}}</wb-icon> <p ng-hide=\"title==undefined || title==null  || title==''\">{{title}}</p> <md-input-container style=\"margin: 0px\"> <input style=\"width: 200px\" ng-model=value flex> </md-input-container> </md-list-item>"
  );

}]);
