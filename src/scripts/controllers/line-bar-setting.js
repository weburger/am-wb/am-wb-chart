/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * general setting for the graph widget
 *
 * author mgh
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartLineBarSettingCtrl', function ($scope) {
	var ngModel = $scope.wbModel;

	// define all graph types which are supported by the widget
	$scope.chartTypes = [{
		title: 'Multi-Line',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'lineChart'
	}, {
		title: 'Bars',
		icon: 'wb-vertical-boxes',
		type: 'single',
		value: 'discreteBarChart'
	}, {
		title: 'Multi-Bars',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'multiBarChart'
	}, {
		title: 'Horizontal Bars',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'multiBarHorizontalChart'
	}];

	/**
	 * creates new data node and adds it to the graph-data which is bound to the nvd3-graph widget
	 */
	$scope.addGraphData = function () {
		if (!ngModel.graphdata)
			ngModel.graphdata = [];
		ngModel.graphdata.push({
			key: 'your-label',
			color: 'red',
			values: {}
		})
	};

	/**
	 * remove a data node from graph-data which is bound to the nvd3-graph widget
	 * @param ds
	 */
	$scope.removeGraphData = function (ds) {
		var index = $scope.wbModel.graphdata.indexOf(ds);
		if (index > -1) {
			$scope.wbModel.graphdata.splice(index, 1);
		}
	}
});