/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Chart-line-bar module
 *
 * author: mgh
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartLineBarWidgetCtrl', function ($scope, $amWbChartPrometheus, $amWbChartDataUtil, $q) {
	var ngModel = $scope.wbModel;

	/*
	 * Chart option,
	 * 
	 * User options must be merged with to show user setting 
	 * effects.
	 */
	if(!angular.isDefined(ngModel.chart)){
		ngModel.chart = {
			type: 'lineChart',
			height: 450,
			margin: {
				top: 50,
				right: 75,
				bottom: 50,
				left: 75
			},
			useVoronoi: false,
			clipEdge: false,
			duration: 250,
			useInteractiveGuideline: true,
			xAxis: {
				showMaxMin: false,
			},
			yAxis: {
				showMaxMin: false,
			}
		} ;
	}
	/**
	 * Load x value
	 */
	ngModel.chart.x = function (data) {
		return data[0];
	};

	/**
	 * Load Y value
	 */
	ngModel.chart.y = function (data) {
		return data[1];
	};

	/**
	 * X-Axis value formatter
	 */
	ngModel.chart.xAxis.tickFormat = function (data) {
		if (!angular.isDefined(ngModel.xformat)){
			return data;
		}
		// Adjust data
		if (ngModel.xformat.adjust) {
			var min = ngModel.data[0].values[0][0];
			ngModel.data.forEach(function (item) {
				if(min > item.values[0][0]){
					min = item.values[0][0];
				}
			});
			data = data - min;
		}
		// Format data
		return $amWbChartDataUtil.formatData(data, ngModel.xformat);
	};

	/**
	 * Y-Axis value formatter
	 */
	ngModel.chart.yAxis.tickFormat = function (data) {
		if (!ngModel.yformat)
			return data;
		return $amWbChartDataUtil.formatData(data, ngModel.yformat);
	};

	/**
	 * Loads default data
	 * 
	 */
	function loadPreData() {
		var result = null;
		if(angular.isDefined(ngModel.query)){
			result = $amWbChartPrometheus.run(ngModel.query, false)//
			.then(function(data){
				ngModel.data = data;
			});
		} else {
			ngModel.data = [{
				key: 'key1',
				color: 'red',
				values: [[0, 0], [1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6,6]]
			}, {
				key: 'key2',
				color: 'blue',
				values: [[0, 0], [1, 2], [2, 4], [3, 8], [4, 16], [5, 32], [6, 64]]
			}];
		}
		if(ngModel.rename){
			$q.when(result)//
			.then(function(){
				angular.forEach(ngModel.data, function(sheet){
					for(var i = 0; i < ngModel.rename.length; i++){
						var rule = ngModel.rename[i];
						var regex = new RegExp(rule.query, 'ig');
						if(regex.test(sheet.key)){
							sheet.key = rule.replace;
							break;
						}
					}
				});
			});
		}
	}

	if (! angular.isArray(ngModel.data)){
		loadPreData();
	}
});
