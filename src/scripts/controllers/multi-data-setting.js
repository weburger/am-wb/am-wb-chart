/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * main setting module for data-query
 *
 * author: mgh
 */
angular.module('ngMaterialWeburgerChart')
/**
 *
 */
.controller('AmWbChartDataMultiSettingCtrl', function($scope, $wbUi) {
	var ngModel = $scope.wbModel;
	
	$scope.chartTypes = [{
		title: 'Line',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'lineChart'
	}, {
		title: 'Bars',
		icon: 'wb-vertical-boxes',
		type: 'single',
		value: 'discreteBarChart'
	}, {
		title: 'Multi-Bars',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'multiBarChart'
	}, {
		title: 'Horizontal Bars',
		icon: 'wb-horizontal-boxes',
		type: 'multi',
		value: 'multiBarHorizontalChart'
	}];

	function addExampleData() {
		if (!angular.isDefined(ngModel.data)) {
			ngModel.data = [];
		}
		ngModel.data.push({
			key : 'New data',
			query : {},
			values : [ [ 1, 1 ], [ 2, 2 ] ]
		});
	}

	function removeDataItem(ds) {
		var index = ngModel.data.indexOf(ds);
		if (index > -1) {
			ngModel.data.splice(index, 1);
		}
	}

	$scope.addData = addExampleData;
	$scope.removeData = removeDataItem;
});