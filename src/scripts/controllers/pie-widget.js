/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

/**
 * Chart-Pie widget module
 *
 * author: mgh
 * @author maso<mostafa.barmshory@dpq.co.ir>
 */
angular.module('ngMaterialWeburgerChart')
	/**
	 *
	 */
	.controller('AmWbChartPieWidgetCtrl', function($scope, $amWbChartPrometheus, $amWbChartDataUtil, $q) {
		var ngModel = $scope.wbModel;

		//default chart options bind to nvd3-graph widget
		if (!angular.isDefined(ngModel)) {
			ngModel = {
				chart : {
					type : 'pieChart',
					height : 450,
					margin : {
						top : 50,
						right : 75,
						bottom : 50,
						left : 75
					},
					showLabels : true,
					labelSunbeamLayout : true
				}
			};
		}

		ngModel.chart.x = function(d) {
			return d[0];
		};

		ngModel.chart.y = function(d) {
			return d[1];
		};

		//check for 'donut' mode
		$scope.$watchCollection('[wbModel.chart.donut]', function() {
			if (ngModel.chart.donut === true) {
				ngModel.chart.pie = {
					startAngle : function(d) {
						return d.startAngle / 2 - Math.PI / 2
					},
					endAngle : function(d) {
						return d.endAngle / 2 - Math.PI / 2
					}
				};
			} else {
				ngModel.chart.pie = {};
			}
		});


		/**
		 * Load sample data
		 */
		function loadPreData() {
			var result = null;
			if(angular.isDefined(ngModel.query)){
				result = $amWbChartPrometheus.run(ngModel.query, false)//
				.then(function(data){
					for(var i = 0; i < data.length; i++){
						$amWbChartDataUtil.appendTextToColumn(data[i].key, 0, data[i]);
					}
					ngModel.data = $amWbChartDataUtil.appendSheetsAsRow(data);
				});
			} else {
				ngModel.data = {
						key : 'Pie sample',
						values : [ [ 'A', 10 ], [ 'B', 10 ], [ 'C', 10 ] ]
				};
			}
			if(ngModel.searchReplace){
				result = $q.when(result)//
				.then(function(){
					if(angular.isArray(ngModel.searchReplace)){
						angular.forEach(ngModel.searchReplace, function(option){
							$amWbChartDataUtil.findAndReplace(ngModel.data, option);
						});
					} else {
						$amWbChartDataUtil.findAndReplace(ngModel.data, ngModel.searchReplace);
					}
				});
			}
		}

		if (!angular.isDefined(ngModel.data)) {
			loadPreData();
		}
	});