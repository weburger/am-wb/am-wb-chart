/**
 * Created by mgh on 2/26/17.
 */
angular.module('ngMaterialWeburgerChart')

    /**
     * @ngdoc directive
     * @name wbUiSettingNumber
     * @memberof ngMaterialWeburger
     * @description a setting section to set a number.
     *
     */
    .directive('wbUiSettingText', function () {
        return {
            templateUrl: 'views/directives/wb-ui-setting-text.html',
            restrict: 'E',
            scope: {
                title: '@title',
                value: '=value',
                icon: '@icon'
            }
        };
    });
