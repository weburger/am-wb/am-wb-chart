/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerChart')

/**
 * @ngdoc service
 * @name $widget
 * @memberof ngMaterialWeburger
 * @description Resource managment
 * 
 */
.service('$amWbChartPrometheus', function($http, $q) {

	/**
	 * Convert string date into unix time
	 */
	function toUnixTime(dstr) {
		var date = moment //
		.utc(dstr);
		return date.unix();
	}

	/**
	 * 
	 */
	function parseValue(value) {
		var val = parseFloat(value);
		if (isNaN(val)) {
			// '+Inf', '-Inf', '+Inf' will be parsed into NaN by parseFloat(). The
			// can't be graphed, so show them as gaps (null).
			return null;
		}
		return val;
	}

	/**
	 * Remove HTML reserved char
	 */
	function escapeHTML(str) {
//		var entityMap = {
//				'&' : '&amp;',
//				'<' : '&lt;',
//				'>' : '&gt;',
//				'\'' : '&#39;',
//				'\"' : '&quot;',
//				'/' : '&#x2F;'
//		};
//
//		return String(string)//
//		.replace(/[&<>''\/]/g, function(s) {
//			return entityMap[s];
//		});
		return str;
	}

	/**
	 * 
	 */
	function metricToTsName(labels) {
		var tsName = (labels.__name__ || '') + '{';
		var labelStrings = [];
		for (var label in labels) {
			if (label !== '__name__') {
				labelStrings.push(label + '=\'' + labels[label] + '\'');
			}
		}
		tsName += labelStrings.join(',') + '}';
		return tsName;
	}
	
	
	
	function join(lookupTable, mainTable, lookupKey, mainKey, select) {
	    var l = lookupTable.length,
	        m = mainTable.length,
	        lookupIndex = [],
	        output = [];
	    for (var i = 0; i < l; i++) { // loop through l items
	        var row = lookupTable[i];
	        lookupIndex[row[lookupKey]] = row; // create an index for lookup table
	    }
	    for (var j = 0; j < m; j++) { // loop through m items
	        var y = mainTable[j];
	        var x = lookupIndex[y[mainKey]]; // get corresponding row from lookupTable
	        // Remove non matche items
	        if(angular.isDefined(x) && angular.isDefined(y)){
	        	output.push(select(y, x)); // select only the columns you need
	        } else {
	        	var w;
	        	w = 1;
	        }
	    }
	    return output;
	};
	
	function concatSheets(sheets){
		if(!angular.isArray(sheets)){
			return sheets;
		}
		if(sheets.length < 2){
			return sheets[0];
		}
		var result = join(sheets[1].values, sheets[0].values, 0, 0, function(row1, row2) {
		    return [
		        row1[0],
		        row1[1],
		        row2[1],
		    ];
		});
		for(var i = 0x2; i < sheets.length; i++){
			result = join(sheets[i].values, result, 0, 0, function(row1, row2) {
			    row1.push(row2[1]);
			    return row1;
			});
		}
		return {
			key: 'Concatedt sheets',
			values: result
		};
	}

	/**
	 * Convert prometheus vector to 2D array:
	 * 
	 *     metric	, value
	 *     x		,x
	 *     
	 * and put it into the sheet format
	 */
	function vectorToSheet(json){
		if (json.resultType !== 'vector') {
			alert('Result is not of matrix type! Please enter a correct expression.');
			return [];
		}
		
		// To data [name, value]
		var values = json.result.map(function(ts) {
			var name;
			if (ts.metric === null) {
				name = 'scalar';
			} else {
				name = escapeHTML(metricToTsName(ts.metric));
			}
			return [name, parseValue(ts.value[1])];
		});
		
		return {
			name: 'vector',
			values: values
		};
	}

	function durationToSeccond(duration){
		// Duration format: [0-9]+[smhdwy]
		if(angular.isNumber(duration)){
			return duration;
		}
		var regex = /([0-9]+)s/gi;
		var match = regex.exec(duration);
		if(match){
			return match[1]*1.0;
		}
		regex = /([0-9]+)m/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60;
		}
		regex = /([0-9]+)h/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60*60;
		}
		regex = /([0-9]+)d/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60*60*24;
		}
		regex = /([0-9]+)w/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60*60*24*7;
		}
		regex = /([0-9]+)y/gi;
		match = regex.exec(duration);
		if(match){
			return match[1]*60*60*24*365;
		}
	}
	
	/**
	 * 
	 */
	function matrixToSheet(params, json) {
		if (json.resultType !== 'matrix') {
			alert('Result is not of matrix type! Please enter a correct expression.');
			return [];
		}
		var data = json.result.map(function(ts) {
			var name;
			if (ts.metric === null) {
				name = 'scalar';
			} else {
				name = escapeHTML(metricToTsName(ts.metric));
			}
			return {
				key : name,
				values : ts.values.map(function(value) {
					return [value[0], parseValue(value[1])];
				}),
			};
		});
		data.forEach(function(s) {
			// Insert nulls for all missing steps.
			var newSeries = [];
			var pos = 0;
			var step = durationToSeccond(params.step);
			for (var t = params.start; t <= params.end; t += step) {
				// Allow for floating point inaccuracy.
				if (s.values.length > pos && s.values[pos][0] < t + step / 100) {
					newSeries.push(s.values[pos]);
					pos++;
				} else {
					newSeries.push([t,null]);
				}
			}
			s.values = newSeries;
		});
		return data;
	}

	/**
	 * Run query based on url.
	 */
	function queryRange(url, params){
		return $http({
			method: 'GET',
			url: url,
			params: params
		}).then(function(data){
			return matrixToSheet(params, data.data.data);
		});
	}

	/**
	 * Run query 
	 */
	function query(url, params){
		return $http({
			method: 'GET',
			url: url,
			params: params
		})
		.then(function(result){
			// maso, 2017: convert result to sheet.
			return vectorToSheet(result.data.data);
		});
	}
	
	function dataSheetToSheet(dataSheet){
		dataSheet.forEach(function(s){
			s.values.unshift(['time', s.key]);
		});
		return concatSheets(data);
	}


	/**
	 * 
	 * params:
	 * 
	 * - url
	 * - start
	 * - end
	 * - step
	 * - query: string or array of string
	 * - queryRange: string
	 * 
	 * 
	 * 
	 */
	function runQuery(param, singleSheet){
		if(angular.isDefined(param.query_range)){
			return queryRange(param.url || '/api/v1/monitor/query_range', {
				query: param.query_range,
				start: toUnixTime(param.start_dtime),
				end: toUnixTime(param.end_dtime),
				step: param.step,
			}).then(function(dataSheet){
				if(singleSheet){
					return dataSheetToSheet(dataSheet);
				}
				return dataSheet;
			});
		}
		if(angular.isArray(param.query)){
			// Run and mearge queries
			var results = [];
			var jobs = [];
			angular.forEach(param.query, function(value){
				jobs.push(query(param.url || '/api/v1/monitor/query',{
					query: value.query,
					time: toUnixTime(param.end_dtime),
				}).then(function(result){
					result.key = value.key || result.key;
					results.push(result);
				}));
			});
			return $q.all(jobs)//
			.then(function(){
				if(singleSheet){
					return dataSheetToSheet(results);
				}
				return results;
			});
		}

		return query(param.url || '/api/v1/monitor/query',{
			query: param.query,
			time: toUnixTime(param.end_dtime),
		});
	}


	this.run = runQuery;
	this.query = query;
	this.queryRange = queryRange;
	this.toUnixTime = toUnixTime;
});
