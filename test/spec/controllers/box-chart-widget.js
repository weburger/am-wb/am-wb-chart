/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

describe('Controllers AmWbCommonFeaturesCtrl', function () {

	// load the directive's module
	beforeEach(module('ngMaterialWeburgerChart'));

//	var $location;
	var $controller;
	var $rootScope;

	beforeEach(inject(function (_$rootScope_, _$controller_) {
//		$location = _$location_;
		$controller = _$controller_;
		$rootScope = _$rootScope_;
	}));


	it('It should replace A with B', inject(function () {
		var scope = $rootScope.$new();
		scope.wbModel = {
				chart : {
					type : 'pieChart',
				},
				searchReplace:{
					query: 'label1',
					replace: 'B',
					match: true,
					regex: true
				}
		};
		$controller('AmWbChartBoxWidgetCtrl', {
			'$scope': scope
		});

	    $rootScope.$apply();
		expect(scope.wbModel.data.values[0][0]).toBe('B');
	}));

	it('It should replace [A-Z]*\d+ with C', inject(function () {
		var scope = $rootScope.$new();
		scope.wbModel = {
				chart : {
					type : 'pieChart',
				},
				searchReplace:[{
					query: '[A-Za-z]+[0-9]+',
					replace: 'C',
					match: true,
					regex: true
				}]
		};
		$controller('AmWbChartBoxWidgetCtrl', {
			'$scope': scope
		});
		
		$rootScope.$apply();
		angular.forEach(scope.wbModel.data.values, function(item){
			expect(item[0]).toBe('C');
		});
	}));
});
